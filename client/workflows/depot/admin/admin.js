

Template.organizations.events({
	'submit #newOrg':function(event){
		orgName = event.target.inputOrgName.value
		orgAddress = event.target.inputAddress.value
		orgSecAddress = event.target.inputSAddress.value
		orgPOCemail = event.target.SecEmail.value

		Meteor.call("addOrganization", orgName,orgAddress,orgSecAddress,orgPOCemail, function(error,result){
			if(error){
				Session.Set('error',true);
			}
			if(result){
				Session.set('error',false)
				Session.set('orgDone',true)
			}
		})
	},
	'click .btn-org-yes':function(){
		Session.set('orgDone',false)
	},
	'click .btn-no':function(){
		Router.go('/depot')
	},
})
Template.organizations.helpers({
	orgIsDone:function(){
		return Session.get('orgDone');
	},
})

Template.userAdmin.helpers({
	orgs: function(){
		return Orgs.find();
	},

	userOrg:function(){
		var userOrgId = this.profile.organization;
		return Orgs.findOne({'_id': userOrgId});
	},

	users:function(){
		return Meteor.users.find();
	},

	userIsDone:function(){
		return Session.get('userDone');
	},

})

Template.userAdmin.events({
	'submit #newUser':function (event) {
		event.preventDefault();
		userName = event.target.inputUserName.value;
		organization = event.target.organizations.value;
		//organization = Orgs.findOne({"orgName": inputOrganization});
		userRole = event.target.role.value
		userEmail = event.target.inputUserEmail.value
		userPhone = event.target.inputUserPhone.value
		Meteor.call("addUser", userName,organization,userEmail,userPhone, function(error,result){
			if(error){
				Session.set('error', true)
			}
			if(result){
				Session.set('error', false)
				Session.set('userDone', true)
				Meteor.call("addRole",result,userRole)
			}
		})//end call
	},//end event
	'click .btn-yes':function(){
		Session.set('userDone', false)
	},
	'click .btn-no':function(){
		Router.go('/depot')
	},
	'click #deleteUser':function(){
		if(window.confirm("Are you sure you want to delete this user from the system")){
			Meteor.call('deleteUser',this._id)
		}
	},


})
