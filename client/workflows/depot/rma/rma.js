//for the /rma/* workflows

Template.createRMA.helpers({
  users:function(){
    return Meteor.users.find();
  },
  orgs:function(){
    return Orgs.find();
  },

  input:function(){
    var inputs =[];
    var amount = Session.get('inputs');
    for (var i = 0; i < amount; i++) {
      inputs.push({index:i});
    }
    return inputs;
  },
});

//
Session.setDefault('inputs', 1)
//

Template.createRMA.events({

  'submit #newRMA':function(event, template){
    event.preventDefault();
    var loggedInUser = Meteor.user();
    var partNumber = event.target.partNumber.value;

//get input from and validate input of serial numbers
    var submittedInputs = [];
    var cleanArray = [];
    for (var i = 0; i < Session.get('inputs'); i++) {
      var fetched = template.find('#input'+i).value;
      submittedInputs.push(fetched);
    }

    submittedInputs.forEach(function(input){
      var regx = /[A-Za-z0-9]{4,12}/;
      if (regx.test(input)) {
        cleanArray.push(input);
      }
      else {
        //maybe do something else here
        console.log(input, "is blank")
      };

     });
    var failureDescription = event.target.failureDescription.value;

    if (Meteor.userId() && !Roles.userIsInRole(loggedInUser,['Admin'])){
      userOrganization = loggedInUser.profile.organization;
			Meteor.call("createRMA",partNumber,cleanArray,failureDescription,userOrganization)
		}
    //get admin values
    else if (Roles.userIsInRole(loggedInUser,['Admin'])) {
      var user = event.target.user.value;
      var assignedOrg = event.target.organization.value;
      var warrantied = event.target.isInWarranty.value;
      Meteor.call("adminCreateRMA",partNumber,cleanArray,failureDescription,user,warrantied,assignedOrg);
    }
    Router.go('/rma')


  },

  'click .addInput':function(){

    Session.set('inputs', Session.get('inputs')+1);

  }
})
/*************************
view RMAs
*************************/


Template.viewRMA.helpers({
  returns:function(){
    return Returns.find()
  },

  returnOwner:function(){
    return Meteor.users.findOne({'_id':this.owner})
  },

  users:function(){
		return Meteor.users.find();
	},

  orgs: function(){
		return Orgs.find();
	},

})

Template.viewRMA.events({
  'click #deleteUser':function(){
    if(window.confirm('are you sure?')){
      Meteor.call('deleteRMA',this._id)
    }
  }
})

/*************************
edit an RMA
*************************/

Template.rmaUpdate.helpers({
  currentUser:function(){
    return Meteor.users.findOne({"_id":this.owner});
  },

  currentOrg:function(){
    //console.log(this)
    return Orgs.findOne({"_id":this.owningOrganization});
    //return thisOrg.orgName;
  },

  users:function(){
    return Meteor.users.find();
  },

  orgs: function(){
    return Orgs.find();
  },

})

Template.rmaUpdate.events({
  'submit #updateRMA':function(event, template){
    event.preventDefault()

    if (event.target.partNumber.value) {
      var partNumber = event.target.partNumber.value;
      Meteor.call('updatePartNumber',partNumber,this);
    }

    for (var i = 0; i < this.serialNumbers.length; i++) {
      if (template.find('#'+i).value) {
        var fetched = template.find('#'+i).value;
        //this is super non-performant but I am going to Meteor.call
        //~i~ will have the value of the serialNumber to be upserted
        Meteor.call('updateSerial',i,fetched,this,function(error,result){console.log(result)});
      }
    }

    if (event.target.failureDescription.value) {
      var failureDescription = event.target.failureDescription.value;
      Meteor.call('updateFailureDescription',failureDescription,this)
    }

    if (event.target.isInWarranty.value){
      var isInWarranty = event.target.isInWarranty.value;
      Meteor.call('updateIsInWarranty',isInWarranty,this)
    }

  }

})
