
Template.depotLayout.events({
	'click .li' : function(event, template){
		var activeLink = template.find('.li')
		if(activeLink){
			activeLink.classList.remove('active')
		}
		event.currentTarget.classList.add('active')
	}
})

Template.orgHeader.helpers({
  userOrg:function(){
    var loggedInUser = Meteor.user();
    userOrganization = Orgs.findOne({"_id":loggedInUser.profile.organization});
    return userOrganization;
  },

  adminOrg:function(){
    var loggedInUser = Meteor.user();
    return loggedInUser.profile.userName;
  }

})

Template.dashboard.nonsenseChart = function() {
    return {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false
        },
        title: {
            text: "Failures"
        },
        tooltip: {
            pointFormat: '<b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    },
                    connectorColor: 'silver'
                }
            }
        },
        series: [{
            type: 'pie',
            name: 'returns',
            data: [
                ['Neglect',   45.0],
                ['Neglect w/ damage',       26.8],
                ['Other',   12.8],
                ['Deactivated',    8.5],
                ['Preventative Maint',     6.2]
            ]
        }]
    };
};
