// Router.configure({
//   layoutTemplate: 'base',
//   loadingTemplate: 'loading'
// });


Router.route('/', function(){
	this.render('landingPage');
})

Router.route('/depot', function(){
	this.layout('depotLayout');
	this.render('dashboard');
})

Router.route('/tracking', function(){
	this.layout('depotLayout');
	this.render('tracking');
})

Router.route('/messaging', function(){
	this.layout('depotLayout');
	this.render('messaging');
})

Router.route('/org', function(){
	this.layout('depotLayout');
	this.render('organizations');
})

Router.route('/user_admin', function(){
	this.wait(Meteor.subscribe('orgs'));

	if(this.ready()){
		this.layout('depotLayout');
		this.render('userAdmin');
	}

})

Router.route('/rma/new', function(){

	this.wait(Meteor.subscribe('users'));
	this.wait(Meteor.subscribe('orgs'));


	if (this.ready()) {
		this.layout('depotLayout');
		this.render('createRMA');
	};

})

Router.route('/rma',function(){

	this.wait(Meteor.subscribe('users'));
	this.wait(Meteor.subscribe('orgs'));
	this.wait(Meteor.subscribe('returns'));

	if (this.ready()) {
		this.layout('depotLayout');
		this.render('viewRMA');
	}


})

Router.route('/rma/:_id',function(){

	this.wait(Meteor.subscribe('users'));
	this.wait(Meteor.subscribe('orgs'));
	this.wait(Meteor.subscribe('returns'));

	this.layout('depotLayout');
	this.render('rmaUpdate',{
		data:function(){
			var something = Returns.findOne({'_id': this.params._id});
			return Returns.findOne({'_id': this.params._id});
		}
	});
})
