

Meteor.methods({

	addOrganization: function(orgName,orgAddress,orgSecAddress,orgPOCemail){

		var loggedInUser = Meteor.users.findOne({_id:this.userId})
		if (loggedInUser || Roles.userIsInRole(loggedInUser,['Admin'])){
			Orgs.insert({
				orgName:orgName,
				orgAddress:orgAddress,
				orgSecAddress:orgSecAddress,
				orgPOCemail:orgPOCemail
			})
			return this._id;
		}
		else{
			throw new Meteor.Error(403,"access denied");
		}

	},

	debug:function(){
		console.log("debug from the server");
	},

	deleteUser:function(userID){
		var loggedInUser = Meteor.users.findOne({"_id":this.userId})

		if (Roles.userIsInRole(loggedInUser,['Admin'])){
			Meteor.users.remove(userID);
		}
		else {
			throw new Meteor.Error(403,"access denied");
		}


	},

	addUser:function(userName,organization,userEmail,userPhone){


		userOptions = {
				email:userEmail,
				profile: {
					userName:userName,
					organization:organization,
					userPhone:userPhone,
				},
		}


		var loggedInUser = Meteor.users.findOne({"_id":this.userId})
		if (Roles.userIsInRole(loggedInUser,['Admin'])){
			var userId= Accounts.createUser(userOptions);
			Accounts.sendEnrollmentEmail(userId);
		}

		//Meteor.users.insert(userId)
		return userId;

	},

	addRole:function(id,userRole){
		//if user is not admin throw error not authoried
		Roles.addUsersToRoles(id, [userRole]);
	},


});



Meteor.publish("users", function(){
	var loggedInUser = Meteor.users.findOne({"_id" : this.userId});


	if (Roles.userIsInRole(loggedInUser._id,['Admin'])){
		return Meteor.users.find();
	}
	else if (Roles.userIsInRole(loggedInUser._id,['User'])) {
		//users should know about other users in their organization
		return Meteor.users.find({"organization":loggedInUser.profile.organization})
	}


});

Meteor.publish("returns", function(){
	var loggedInUser = Meteor.users.findOne({"_id" : this.userId});


	if (Roles.userIsInRole(loggedInUser._id,['User'])){
		//var users_in_same_org = Meteor.users.find({"organization":loggedInUser.organization}).fetch()
		return Returns.find({"owningOrganization":loggedInUser.profile.organization});
	} else if (Roles.userIsInRole(loggedInUser._id,['Admin'])) {
		return Returns.find();
	}


});

Meteor.publish("orgs", function(){
	

	var loggedInUser = Meteor.users.findOne({"_id" : String(this.userId)});


	if (Roles.userIsInRole(loggedInUser._id,['Admin'])){
		return Orgs.find();
	}
	else if (Roles.userIsInRole(loggedInUser._id,['User'])) {
		return Orgs.find({"_id":loggedInUser.profile.organization});
	}


});
