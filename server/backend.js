//user auth https://github.com/alanning/meteor-roles/



	Meteor.startup(function(){
		console.log("server is starting up")


		// EMAIL ADDRESS OF PRIME ADMIN - change this to become master of the universe
		var primeAdminEmail = 'dtzitz@live.com';
		var adminName = "Admin";
		var organization = "Administrators";
		var userPhone = "1234567";

		//organization info of the admin
		var orgName = "Administrators";
		var orgAddress = "123 Sesame St. Tampa FL 33089";
		var orgSecAddress = "123 Sesame St. Tampa FL 33089";
		var orgPOCemail = primeAdminEmail;
		//check to see if admin exists before trying to create it

		var adminExists = Meteor.users.findOne({"emails.address":primeAdminEmail})
		if (!adminExists){

			//need the create Administrators organization to put it's id
			var orgID = Orgs.insert({
				orgName:orgName,
				orgAddress:orgAddress,
				orgSecAddress:orgSecAddress,
				orgPOCemail:orgPOCemail
			},function(error,result){
				if(error){
					console.log("There was an error on starup with "+ this);
				}
				else {
					return this._id;
				}
			});

			adminSetup = {
				email:primeAdminEmail,
				profile:{
					userName:adminName,
					organization:orgID,
					userPhone:userPhone,
				}
			}


			var userId = Accounts.createUser(adminSetup);
			Accounts.sendEnrollmentEmail(userId);
			var newAdmin = Meteor.users.findOne({"emails.address":primeAdminEmail})
			Roles.addUsersToRoles(newAdmin, 'Admin');
		}
		console.log("server is up")

	});



	//Meteor.publish some stuff here
	Meteor.methods({
		sendEmail: function(email){
			var all_emails = EmailAddress.find().fetch();
			EmailAddress.insert({
				emailaddress : email,
			})

			Email.send({
				to:email,
				from:'no-reply@depotpls.com',
				subject: 'Thank you for following our project',
				text:"I won't spam you with newsletters or sell your email address. The next email you get from us will be to notify you of the site progress",
			});

			Email.send({
				to:primeAdminEmail,
				from:'newrequest@depotpls.com',
				subject:'new access requested',
				text:'You have a request from ' + email + ' to access the site. <br> also ' + JSON.stringify(all_emails,['emailaddress'], '     ')
			});

		},

	});
