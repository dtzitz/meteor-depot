
Meteor.methods({

  createRMA: function(partNumber,cleanArray,failureDescription,userOrganization){

    var loggedInUser = Meteor.users.findOne({"_id":this.userId})

    var isInWarranty = false; //default value for now will need to do a workflow defining parts and their warranty
    if (Roles.userIsInRole(loggedInUser,['User'])){
      Returns.insert({
        partNumber: partNumber,
        serialNumbers: cleanArray,
        failureDescription: failureDescription,
        owner: Meteor.userId(),
        owningOrganization: userOrganization,
        isInWarranty: isInWarranty,
      });

    }
    else {
      throw new Meteor.Error(403,"access denied");
    };

  },

  updateSerial:function(iter,fetched,returnObj){
    var loggedInUser = Meteor.users.findOne({"_id":this.userId});
    if (Roles.userIsInRole(loggedInUser,['Admin','User'])) {
      //programatically build a setModifier because mongo is stupid and can't concat strings
      var setModifier = {$set:{}};
      setModifier.$set['serialNumbers.'+iter] = fetched;
      Returns.update({'_id':returnObj._id},setModifier,function(error,result){return result;})
    }
  },

  updatePartNumber:function(partNumber,returnObj){
    var loggedInUser = Meteor.users.findOne({"_id":this.userId});
    if (Roles.userIsInRole(loggedInUser,['Admin','User'])) {
      Returns.update({'_id':returnObj._id},{$set:{'partNumber':partNumber}})
    }
  },

  updateFailureDescription:function(failureDescription,returnObj){
    var loggedInUser = Meteor.users.findOne({"_id":this.userId});
    if (Roles.userIsInRole(loggedInUser,['Admin','User'])) {
      Returns.update({'_id':returnObj._id},{$set:{'failureDescription':failureDescription}})
    }
  },

  updateIsInWarranty:function(isInWarranty,returnObj){
    var loggedInUser = Meteor.users.findOne({"_id":this.userId});
    if (Roles.userIsInRole(loggedInUser,['Admin'])) {
      Returns.update({'_id':returnObj._id},{$set:{'isInWarranty':isInWarranty}});
    }
  },


  deleteRMA:function(returnID){
    var loggedInUser = Meteor.users.findOne({"_id":this.userId})

    if (Roles.userIsInRole(loggedInUser,['Admin','User'])) {
      Returns.remove(returnID);
    }
  },

  adminCreateRMA: function(partNumber,cleanArray,failureDescription,user,warrantied,assignedOrg){

    var loggedInUser = Meteor.users.findOne({"_id":this.userId});
    var isInWarranty = false; //default value for now will need to do a workflow defining parts and their warranty
    var owner = Meteor.users.find( {"profile.userName": user}).fetch();
    if (Roles.userIsInRole(loggedInUser,['Admin'])){
      Returns.insert({
        partNumber: partNumber,
        serialNumbers: cleanArray,
        failureDescription: failureDescription,
        owner: owner[0]._id, // this is an array because Meteor.users.find instead of Meteor.users.findOne
        isInWarranty: warrantied,
        dateOpened: moment().format('L'),
        owningOrganization: assignedOrg,

      });

    }
    else {
      throw new Meteor.Error(403,"access denied");
    };
  }



})
