## Synopsis

This is a Meteor application that will provide inventory tracking and depot (RMA) management


## Motivation

I had to use a really bad ASP.net classic application for the longest time. Wanted to build a better tool for myself and others

## Installation

Go get Meteor and clone this project into the directory. Change the email address on lines 9, 12, and 19 to your own email address to become THE MASTER OF THE UNIVERSE... err the first admin. Application is designed to be invite only.

## API Reference

TBD

## Tests

I know I should write these. I feel really bad about not doing it. Honest.

## Contributors

If you want to get in on this let me know my username is also my twitter handle

## License

Copyright (c) 2015 Dylan Zitzmann
The MIT License (MIT)


Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
